<?

class History {

	function History(){}

	function record(){
		global $_SESSION, $_SERVER;

		$skip = false;

		// filename
		$filename = basename($_SERVER['PHP_SELF']);
		if(strpos($filename, '?'))
			$filename = substr($filename, 0, strpos($filename, '?'));
		$last_filename = $_SESSION['history'][count($_SESSION['history'])-1];

		// last filename
		if(strpos($last_filename, '?'))
			$last_filename =
				substr($last_filename, 0, strpos($last_filename, '?'));

		// we need to skip the entry if the page was loaded 5 times in a row,
		// for instance, which can be true if you load the menu.php frame a lot
		if($filename == $last_filename)
			$skip = true;

		// create the record
		if(!$skip)
			$_SESSION['history'][] = "{$filename}?{$_SERVER['QUERY_STRING']}";
	}

	function go_back(){
		header('Location: '.$this->remove_last());
	}

	function remove_last(){
		global $_SESSION;
		$url = $_SESSION['history'][count($_SESSION['history'])-2];
		unset($_SESSION['history'][count($_SESSION['history'])-1]);
		return $url;
	}

	function blank(){
		return (count($_SESSION['history'])<=1);
	}

	function clear(){
		unset($_SESSION['history']);
		$_SESSION['history'] = array();
	}

}

?>
