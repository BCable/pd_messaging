<?

class Config {

	// version
	const VERSION = '2.1.0';
	const SCRIPT_URL = '<absolute_url_path>';

	// database config
	const DB_HOST = 'localhost';
	const DB_USER = 'user';
	const DB_PASS = 'pass';
	const DB_NAME = 'name';

	// colors
	const COLOR_GLOBAL = '#FF5353';
	const COLOR_REGION = '#6FFF44';
	const COLOR_SECTOR = '#7373FF';
	const COLOR_SECURE = '#C8C8C8';
}

?>
