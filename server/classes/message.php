<?
include_once('classes/generic.php');
include_once('classes/paging.php');
include_once('classes/region.php');
include_once('classes/user.php');

class Message extends Generic {

	var $stored = false;
	var $data_name = 'msg';

	var $paging;
	var $region;
	var $user;

	var $id;
	var $from_id;
	var $to_id;
	var $recipients;
	var $methods;
	var $sector;
	var $date;
	var $expire_on;
	var $msg_seen;
	var $subject;
	var $body;

	function Message(){}

	function class_store($ret){
		$this->id = $ret['msg_id'];
		$this->from_id = $ret['msg_from_id'];
		$this->to_id = $ret['msg_to_id'];
		$this->recipients = $ret['msg_recipients'];
		$this->methods = $ret['msg_methods'];
		$this->sector = $ret['msg_sector'];
		$this->date = $ret['msg_date'];
		$this->expire_on = $ret['msg_expire_on'];
		$this->msg_seen = $ret['msg_seen'];
		$this->expire_on = $ret['msg_expire_on'];
		$this->subject = $ret['msg_subject'];
		$this->body = $ret['msg_body'];
		$this->user = new User();
		$this->user = $this->user->get_object($ret);
		$this->region = new Region();
		$this->region = $this->region->get_object($ret);
		$this->stored = true;
		return $this;
	}

	function set_paging(&$paging){
		$this->paging = $paging;
	}

	function get_by_id($id){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN alliance AS a ON a.alliance_id = u.user_alliance_id
			LEFT JOIN region AS r ON m.msg_region_id = r.region_id
			WHERE msg_id = %d
			LIMIT 1', $id
		));
		return $this->get_object($ret);
	}

	function get_type(){
		if(!empty($this->sector))
			return 'sector';
		elseif(!empty($this->region->id))
			return 'region';
		elseif($this->to_id == -1)
			return 'global';
		else
			return 'secure';
	}

	function get_printable_type(){
		$type = ucfirst($this->get_type());
		if($type == 'Region')
			$type .= 'al';
		return $type;
	}

	function mark_read($user, $msg){
		global $sql;

		if($_SESSION['incognito'])
			return true;

		if(!strpos($msg->msg_seen, "|{$user->id}|")){
			// if it's null, just make it the user's id (one recipient)
			if($msg->msg_seen == null){
				$sql->query_to(sprintf('
					UPDATE message
					SET msg_seen = "|%d|"
					WHERE msg_id = %d',
					$user->id, $msg->id
				));

			// multiple recipients
			} else {
				$sql->query_to(sprintf('
					UPDATE message
					SET msg_seen = CONCAT(msg_seen, "%d|")
					WHERE
						msg_id = %d AND
						msg_seen NOT LIKE "%%|%d|%%"',
					$user->id, $msg->id, $user->id
				));
			}
		}
	}

	function mark_all_read($user, $messages){
		global $sql;
		foreach($messages as $msg){
			$this->mark_read($user, $msg);
		}
		return $sql->success();
	}

	// GLOBAL MESSAGES {{{

	function get_global(){
		global $sql;
		$limit = ($this->paging?$this->paging->get_sql_limit():'');
		$ret = $sql->query(sprintf('
			SELECT * FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN alliance AS a ON a.alliance_id = u.user_alliance_id
			WHERE msg_to_id = -1
			ORDER BY msg_date DESC %s', $limit));
		return $this->get_objects($ret);
	}

	function get_global_cnt(){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message
			WHERE msg_to_id = -1'
		));
		return intval($ret['cnt']);
	}

	function get_global_cnt_new($user){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message
			WHERE msg_to_id = -1 AND msg_date > %d', $user->last_update
		));
		return intval($ret['cnt']);
	}

	// }}}

	// SECURE MESSAGES {{{

	function get_secure($user){
		global $sql;
		$limit = ($this->paging?$this->paging->get_sql_limit():'');
		$ret = $sql->query(sprintf('
			SELECT * FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN alliance AS a ON a.alliance_id = u.user_alliance_id
			WHERE msg_to_id = %d
			ORDER BY msg_date DESC %s', $user->id, $limit
		));
		return $this->get_objects($ret);
	}

	function get_secure_cnt($user){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message
			WHERE msg_to_id = %d', $user->id
		));
		return intval($ret['cnt']);
	}

	function get_secure_cnt_new($user){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message
			WHERE msg_to_id = %d AND msg_seen IS NULL', $user->id
		));
		return intval($ret['cnt']);
	}

	// }}}

	// SECTOR MESSAGES {{{

	function get_sector($sector){
		global $sql;
		$limit = ($this->paging?$this->paging->get_sql_limit():'');
		$ret = $sql->query(sprintf('
			SELECT * FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN alliance AS a ON a.alliance_id = u.user_alliance_id
			WHERE msg_sector = \'%s\' AND
				(msg_expire_on > %d OR msg_expire_on = msg_expire_on)
			ORDER BY msg_date DESC %s',
			mysql_real_escape_string($sector->name), gmmktime(), $limit
		));
		return $this->get_objects($ret);
	}

	function get_sector_cnt($sector){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message
			WHERE msg_sector = \'%s\' AND
				(msg_expire_on > %d OR msg_expire_on = msg_expire_on)',
			mysql_real_escape_string($sector->name), gmmktime()
		));
		return intval($ret['cnt']);
	}

	function get_sector_cnt_new($user, $sector){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message
			WHERE
				msg_sector = \'%s\' AND
				msg_seen NOT LIKE "%%|%d|%%" AND
				(msg_expire_on > %d OR msg_expire_on = msg_expire_on)',
			mysql_real_escape_string($sector->name), $user->id, gmmktime()
		));
		return intval($ret['cnt']);
	}

	// }}}

	// REGIONAL MESSAGES {{{

	function get_region($sector){
		global $sql;
		$limit = ($this->paging?$this->paging->get_sql_limit():'');
		$ret = $sql->query(sprintf('
			SELECT * FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN alliance AS a ON a.alliance_id = u.user_alliance_id
			JOIN region AS r ON m.msg_region_id = r.region_id
			WHERE r.region_sectors LIKE "%%|%s|%%"
			ORDER BY msg_date DESC %s',
			mysql_real_escape_string($sector->name), $limit
		));
		return $this->get_objects($ret);
	}

	function get_region_cnt($sector){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN region AS r ON m.msg_region_id = r.region_id
			WHERE r.region_sectors LIKE "%%|%s|%%"
			GROUP BY m.msg_id',
			mysql_real_escape_string($sector->name)
		));
		return intval($ret['cnt']);
	}

	function get_region_cnt_new($user, $sector){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT COUNT(*) AS cnt FROM message AS m
			JOIN user AS u ON m.msg_from_id = u.user_id
			JOIN region AS r ON m.msg_region_id = r.region_id
			WHERE
				r.region_sectors LIKE "%%|%s|%%" AND
				msg_seen NOT LIKE "%%|%d|%%"
			GROUP BY m.msg_id',
				mysql_real_escape_string($sector->name),
				$user->id
		));
		return intval($ret['cnt']);
	}

	// }}}

	// SEND MESSAGES {{{

	function send_regular($from, $to, $recipients, $methods, $subject, $body){
		global $sql;

		// check if user exists
		$user = new User();
		$user->get_by_name($to);
		if(!$user->id || empty($user->pass))
			return false;

		// if user exists, create the message!
		$sql->query_to(sprintf('
			INSERT INTO message (
				msg_from_id, msg_to_id, msg_recipients, msg_methods,
				msg_date, msg_subject, msg_body
			) VALUES (
				%d, %d, "%s", "%s", %d, "%s", "%s"
			)',
			$from->id, $user->id, $recipients, $methods, gmmktime(),
			mysql_real_escape_string($subject),
			mysql_real_escape_string($body)
		));

		return $sql->success();
	}

	function send_global($from, $subject, $body){
		global $sql;

		if(!$from->has_permission('GLOBAL'))
			return false;

		// create the message
		$sql->query_to(sprintf('
			INSERT INTO message (
				msg_from_id, msg_to_id, msg_date,
				msg_subject, msg_body
			) VALUES (
				%d, -1, %d, "%s", "%s"
			)',
			$from->id, gmmktime(),
			mysql_real_escape_string($subject),
			mysql_real_escape_string($body)
		));

		return $sql->success();
	}

	function send_sector($from, $sector, $expires_in, $subject, $body){
		global $sql;

		if(!$from->has_permission('SECTOR', $sector))
			return false;

		// create the message
		$sql->query_to(sprintf('
			INSERT INTO message (
				msg_from_id, msg_date, msg_seen,
				msg_expire_on, msg_sector, msg_subject, msg_body
			) VALUES (
				%d, %d, "|", %d, "%s", "%s", "%s"
			)',
			$from->id, gmmktime(), gmmktime()+intval($expires_in)*3600,
			mysql_real_escape_string($sector),
			mysql_real_escape_string($subject),
			mysql_real_escape_string($body)
		));

		return $sql->success();
	}

	function send_region($from, $region_id, $expires, $subject, $body){
		global $sql;

		if(!$from->has_permission('REGION', $region_id))
			return false;

		// create the message
		$sql->query_to(sprintf('
			INSERT INTO message (
				msg_from_id, msg_region_id, msg_date, msg_seen,
				msg_expire_on, msg_subject, msg_body
			) VALUES (
				%d, %d, %d, "|", %d, "%s", "%s"
			)',
			$from->id, $region_id, gmmktime(), gmmktime()+intval($expires)*3600,
			mysql_real_escape_string($subject),
			mysql_real_escape_string($body)
		));

		return $sql->success();
	}

	// }}}

	function has_permission($user){
		$type = $this->get_type();
		switch($type){
			case 'secure':
				return true;
				break;
			case 'sector':
				return $user->has_permission('SECTOR', $this->sector);
				break;
			case 'region':
				return $user->has_permission('REGION', $this->region->id);
				break;
			case 'global':
				return $user->has_permission('GLOBAL');
				break;
			default:
				return false;
				break;
		}
	}

	function update($user){
		global $sql;

		if(!$this->has_permission($user))
			return false;

		$sql->query_to(sprintf('
			UPDATE message
			SET
				msg_subject = "%s",
				msg_body = "%s"
			WHERE
				msg_id = %d
			LIMIT 1',
			mysql_real_escape_string($this->subject),
			mysql_real_escape_string($this->body),
			$this->id
		));

		return $sql->success();
	}

	function delete($user){
		global $sql;

		if(!$this->has_permission($user))
			return false;

		$sql->query_to(sprintf('
			DELETE FROM message
			WHERE
				msg_id = %d', $this->id
		));
	}

	function render($user, $type = false){
		$date = substr(gmdate('r', $this->date), 0, -6);

		// determine access, to see if they get an edit button
		if($this->get_type() == 'secure'){
			$access = ($user->id == $this->user->id);
		} else {
			$access = $this->has_permission($user);
		}
?>
<table cellpadding="5" cellspacing="0" class="msg_page">
	<tr class="first">
		<th>From:</th>
		<td>
			<?=$this->user->render()?>
		</td>
	</tr>
	<tr>
		<th>Alliance:</th>
		<td><?=htmlentities($this->user->alliance->name)?></td>
	</tr>
<?	if(!empty($this->sector)){ ?>
	<tr>
		<th>Sector:</th>
		<td><?=htmlentities($this->sector)?></td>
	</tr>
<?	}
	if(!empty($this->region->name)){ ?>
	<tr>
		<th>Region:</th>
		<td><?=htmlentities($this->region->name)?></td>
	</tr>
<?	}
	if(!empty($this->recipients)){ ?>
	<tr>
		<th>Recipients:</th>
		<td><?
			$recipients = explode(', ', $this->recipients);
			$first = true;
			foreach($recipients as $recipient){
				if(!$first) echo ', ';
				else $first = false;
				$rpt = new User();
				$rpt->name = $recipient;
				$rpt->render();
			}
		?>
		</td>
	</tr>
<?	}
	if(!empty($this->methods)){ ?>
	<tr>
		<th>Send&nbsp;Methods:</th>
		<td><?=htmlentities($this->methods)?></td>
	</tr>
<?	} ?>
	<tr>
		<th>Subject:</th>
		<td><?=htmlentities($this->subject)?></td>
	</tr>
	<tr class="last">
		<th>Date&nbsp;(GMT):</th>
		<td><?=$date?></td>
	</tr>
</table>
<?	if($access){ ?>
<div class="fright">
	<a href="edit_message.php?id=<?=$this->id?>">Edit&nbsp;Message</a>
</div>
<?	} ?>
<br />

<div id="msg_body"><?=nl2br($this->body)?></div>

<br />
<hr />
<? }

}

?>
