<?

include_once('classes/generic.php');

class Permission extends Generic {

	var $name;
	var $data;

	function Permission(){}

	function class_store($ret){
		$this->name = $ret['perm_name'];
		$this->data = $ret['perm_data'];
		$this->stored = true;
		return $this;
	}

	function get_perms_by_name($name){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM permission
			WHERE perm_name = \'%s\'',
			mysql_real_escape_string($name)
		));
		return $this->get_objects($ret);
	}

	function get_perms_by_name_val($name, $val){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM permission
			WHERE perm_name = "%s" AND perm_data = "%s"',
			mysql_real_escape_string($name),
			mysql_real_escape_string($val)
		));
		return $this->get_objects($ret);
	}

	function get_perms_by_user_id($id){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM permission
			WHERE perm_user_id = %d', $id
		));
		return $this->get_objects($ret);
	}

}

?>
