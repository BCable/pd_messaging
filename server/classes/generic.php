<?

class Generic {

	// required class to be overridden; commented out to break in PHP4
	//function class_store(){}

	// get all elements as an array of objects
	function get_objects($ret){
		$objects = array();
		foreach($ret as $row){
			$objects[] = clone $this->class_store($row);
		}
		return $objects;
	}

	// get all elements as an object
	function get_object($ret){
		return $this->class_store($ret);
	}

	// get all elements as an array
	function get_array(){
		$array = get_object_vars($this);
		$ret = array();
		foreach($array as $key=>$val){
			if(!empty($this->data_name)){
				$ret[strtolower($this->data_name)."_{$key}"] = $val;
			} else {
				$ret[strtolower(get_class($this))."_{$key}"] = $val;
			}
		}
		return $ret;
	}

}

?>
