<?

class QuickSQL {

	var $connection;
	var $result;

	function connect($host, $user, $pass){
		$this->connection =
			mysql_connect($host, $user, $pass) or die(mysql_error());
	}

	function close(){
		mysql_close($this->connection) or die(mysql_error());
	}

	function select_db($database){
		mysql_select_db($database, $this->connection) or die(mysql_error());
	}

	function query_to($qry){
		$this->result =
			mysql_query($qry, $this->connection) or die(mysql_error());
	}

	function query_one($qry){
		$this->result =
			mysql_query($qry, $this->connection) or die(mysql_error());
		return @mysql_fetch_assoc($this->result);
	}

	function query($qry){
		$this->result =
			mysql_query($qry, $this->connection) or die(mysql_error());

		$ret = array();
		while($row = @mysql_fetch_assoc($this->result)){
			$ret[] = $row;
		}

		return $ret;
	}

	function num_rows($qry){
		$this->result =
			mysql_query($qry, $this->connection) or die(mysql_error());
		return intval(@mysql_num_rows($this->result));
	}

	function step(){
		return @mysql_fetch_assoc($this->result);
	}

	function success(){
		return (mysql_error()=='');
	}

}

?>
