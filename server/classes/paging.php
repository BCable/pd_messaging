<?

class Paging {

	var $limit;
	var $page;
	var $total;

	function Paging($page, $limit){
		$this->set_page(intval($page));
		$this->limit = intval($limit);
		if($this->limit == 0){
			$this->limit = 3;
		}
	}

	// set page
	function set_page($page){
		if(empty($page))
			$this->page = 1;
		else
			$this->page = $page;
	}

	// calculate offset
	function get_sql_start(){
		return ($this->page-1)*$this->limit;
	}

	// set total
	function set_total($total){
		$this->total = $total;
	}

	// get limit sql
	function get_sql_limit(){
		return "LIMIT {$this->get_sql_start()},{$this->limit}";
	}

	// print page trail
	function print_page_trail($file = '', $query = ''){
		// display page numbers
		$total_pages = ceil($this->total/$this->limit);
		if(!empty($query))
			$query = "{$query}&";

		echo 'Pages:&nbsp;';

		if($total_pages == 0){
			echo '1';
		} else {
			for($i=1; $i <= $total_pages; $i++){
				if($i > 0){
					echo '&nbsp;';
				}

				if($i != $this->page){
					echo "<a href=\"{$file}?{$query}p={$i}\">{$i}</a>";
				} else {
					echo "({$this->page})";
				}
			}
		}
	}

}

?>
