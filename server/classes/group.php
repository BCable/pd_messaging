<?

include_once('classes/generic.php');

class Group extends Generic {

	var $stored = false;

	var $user;

	var $id;
	var $name;
	var $user_id;
	var $users;

	function Group(){}

	function class_store($ret){
		$this->id = $ret['group_id'];
		$this->name = $ret['group_name'];
		$this->user = new User();
		$this->user = $this->user->get_object($ret);
		$this->users = $ret['group_users'];
		$this->stored = true;
		return $this;
	}

	function get_groups($user){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM `group`
			WHERE group_user_id = %d', $user->id
		));
		return $this->get_objects($ret);
	}

	function get_by_id($id){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM `group`
			WHERE group_id = %d', $id
		));
		return $this->get_object($ret);
	}

	function create(){
		global $sql;
		$sql->query_to(sprintf("
			INSERT INTO `group`
			(group_name, group_user_id, group_users)
			VALUES
			('%s', %d, '%s')",
			mysql_real_escape_string($this->name),
			$this->user->id,
			mysql_real_escape_string($this->users)
		));
	}

	function delete($user){
		global $sql;
		$sql->query_to(sprintf('
			DELETE FROM `group`
			WHERE group_id = %d AND group_user_id = %d',
			$this->id, $user->id
		));
	}

	function update($user){
		global $sql;
		$sql->query_to(sprintf("
			UPDATE `group`
			SET
				group_name = '%s',
				group_users = '%s'
			WHERE
				group_id = %d AND
				group_user_id = %d
			LIMIT 1",
			mysql_real_escape_string($this->name),
			mysql_real_escape_string($this->users),
			$this->id, $user->id
		));
	}

}

?>
