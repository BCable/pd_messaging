<?
include_once('classes/generic.php');

class Alliance extends Generic {

	var $stored = false;

	var $id;
	var $universe;
	var $name;

	function Alliance(){}

	function class_store($ret){
		$this->id = $ret['alliance_id'];
		$this->name = $ret['alliance_name'];
		$this->stored = true;
		return $this;
	}

	function get_by_id($id, $store = true){
		global $sql;
		if($this->stored)
			return true;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM alliance
			WHERE alliance_id = %d', $id
		));
		if($store)
			$this->class_store($ret);
		return $this->get_object($ret);
	}

	function get_by_name($name, $store = true){
		global $sql;
		if($this->stored)
			return true;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM alliance
			WHERE alliance_name = \'%s\'',
				mysql_real_escape_string($name)
		));
		if($store)
			$this->class_store($ret);
		return $this->get_object($ret);
	}

	function get_alliances(){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM alliance
			ORDER BY alliance_name ASC
		'));
		return $this->get_objects($ret);
	}

}

?>
