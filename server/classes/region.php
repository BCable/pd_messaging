<?
include_once('classes/generic.php');

class Region extends Generic {

	var $stored = false;

	var $id;
	var $name;
	var $sectors;

	function Region(){}

	function class_store($ret){
		$this->id = $ret['region_id'];
		$this->name = $ret['region_name'];
		$this->sectors = $ret['region_sectors'];
		$this->stored = true;
		return $this;
	}

	function get_by_id($id){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM region
			WHERE region_id = %d
			LIMIT 1', $id
		));
		return $this->get_object($ret);
	}

	function get_by_sector_name($name){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM region
			WHERE region_sectors LIKE "%%|%s|%%"
			LIMIT 1',
			mysql_real_escape_string($name)
		));
		return $this->get_object($ret);
	}

	function get_regions($sector){
		global $sql;
		$ret = $sql->query(sprintf("
			SELECT * FROM region
			WHERE region_sectors LIKE '%%|%s|%%'
			ORDER BY region_name ASC",
			mysql_real_escape_string($sector->name)
		));
		return $this->get_objects($ret);
	}

	function get_all_regions(){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM region
			ORDER BY region_name ASC
		'));
		return $this->get_objects($ret);
	}

	function has_sector($name){
		return strpos($this->sectors, "|{$name}|");
	}

	function get_regions_by_permissions($user){
		// admins access all regions
		if($user->has_permission('GLOBAL')){
			$region = new Region();
			$regions = $region->get_all_regions();
		}

		// other users get different amounts of regions
		$region_ids = $user->get_perms_by_key('REGION');

		// store regions into array
		foreach($region_ids as $region_id){
			$region = new Region();
			$regions[] = $region->get_by_id($region_id);
		}

		// other users get different amounts of sectors
		$sector_names = $user->get_perms_by_key('SECTOR');

		// get region name associated with sector
		foreach($sector_names as $sector_name){
			$region = new Region();
			$region->get_by_sector_name($sector_name);

			// make it look like a region
			$region->sectors = "|{$sector_name}|";
			$region->no_region = true;
			$regions[] = $region;
		}

		// sort array
		function region_sort($a, $b){
			return $a->name > $b->name;
		}
		usort($regions, 'region_sort');

		return $regions;
	}

}
