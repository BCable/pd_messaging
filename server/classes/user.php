<?
include_once('classes/alliance.php');
include_once('classes/generic.php');
include_once('classes/sector.php');
include_once('classes/permission.php');

class User extends Generic {

	var $stored = false;
	var $perms_stored = false;

	var $alliance;

	var $id;
	var $universe;
	var $name;
	var $alliance_id;
	var $pass;
	var $last_update;

	var $perms;

	function User(){}

	function class_store($ret){
		$this->id = $ret['user_id'];
		$this->name = $ret['user_name'];
		$this->universe = $ret['user_universe'];
		$this->alliance_id = $ret['user_alliance_id'];
		$this->pass = $ret['user_pass'];
		$this->last_update = $ret['user_last_update'];
		$this->alliance = new Alliance();
		$this->alliance = $this->alliance->get_object($ret);
		$this->stored = true;
		return $this;
	}

	function get_by_id($id, $store = true){
		global $sql;
		if($this->stored)
			return true;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM user
			WHERE user_id = %d', $id
		));
		if($store)
			$this->class_store($ret);
		return $this->get_object($ret);
	}

	function get_by_name($name, $store = true){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM user
			WHERE user_name = \'%s\'',
				mysql_real_escape_string($name)
		));
		if($store)
			$this->class_store($ret);
		return $this->get_object($ret);
	}

	function has_permission($name, $val = false){
		$perm = new Permission();

		if(!$this->perms_stored){
			$this->perms = $perm->get_perms_by_user_id($this->id);
			$this->perms_stored = true;
		}

		$has_perm = false;
		foreach($this->perms as $perm){
			// admin match
			if($perm->name == 'ADMIN' || $perm->name == 'GLOBAL'){
				$has_perm = true;
			}

			// permission match
			if($perm->name == $name && (
				$perm->data == $val ||
				$perm->data == -1 ||
				$val === false
			)) $has_perm = true;

			// sector match
			if(!$has_perm && $perm->name == 'REGION' && $name == 'SECTOR'){
				$region = new Region();
				$region->get_by_id($perm->data);
				if($region->has_sector($val))
					$has_perm = true;
			}

			// banned
			if($perm->name == 'BAN'){
				$has_perm = false;
				break;
			}
		}

		return $has_perm;
	}

	function is_admin(){
		$perm = new Permission();

		if(!$this->perms_stored){
			$this->perms = $perm->get_perms_by_user_id($this->id);
			$this->perms_stored = true;
		}

		$has_perm = false;
		foreach($this->perms as $perm){
			// admin match
			if($perm->name == 'ADMIN'){
				$has_perm = true;
				break;
			}
		}

		return $has_perm;
	}

	function update_global($user = null){
		if($_SESSION['incognito'])
			return true;

		if($user == null) $user = $this;
		global $sql;
		$time = gmmktime();
		$ret = $sql->query_to(sprintf("
			UPDATE user SET user_last_update = {$time}
			WHERE user_id = %d", $user->id
		));
		return $ret;
	}

	function login($username, $password, $skip_die = false){
		$ret = $this->get_by_name($username, false);
		// admin login as someone else
		if(!empty($_SESSION['admin_username'])){
			$_SESSION['incognito'] = true;
			$ret = $this->get_by_name($_SESSION['admin_username'], false);
			$this->class_store($ret->get_array());
			return true;

		// regular login
		} elseif($ret->pass == sha1($password)){
			$this->class_store($ret->get_array());
			$this->log_ip();
			return true;

		// failed login
		} else {
			if(!$skip_die)
				die('Login failed');
			return false;
		}
	}

	function require_perm($perm, $data = false){
		if(!$this->has_permission($perm, $data)){
			die('Permission denied.');
		}
	}

	function require_perm_array_or($perm, $data){
		$has_one = false;
		for($i=0; $i<count($perm); $i++){
			if(!$this->has_permission($perm[$i], $data[$i])){
				$has_one = true;
				break;
			}
		}
		return true;
	}

	function get_perms_by_key($perm_name){
		$vals = array();
		foreach($this->perms as $perm){
			if($perm_name == $perm->name){
				$vals[] = $perm->data;
			}
		}
		return $vals;
	}

	function login_auth($auth, $skip_die = false){
		$auth_dec = urldecode(base64_decode($auth));
		$username = mysql_real_escape_string(
			substr($auth_dec, 0, strpos($auth_dec, ':'))
		);
		$password = substr($auth_dec, strpos($auth_dec, ':')+1);
		if($this->login($username, $password, $skip_die)){
			setcookie('auth', $auth);
			return true;
		}
		else return false;
	}

	function log_ip(){
		global $sql;
		$sql->query_to(sprintf('
			UPDATE user
			SET user_ips = CONCAT(user_ips, "%s|")
			WHERE
				user_id = %d AND
				user_ips NOT LIKE "%%|%s|%%"',
			$_SERVER['REMOTE_ADDR'], $this->id, $_SERVER['REMOTE_ADDR']
		));
	}

	function register_sector(){
		global $_GET, $_POST, $_REQUEST;

		$sector = null;
		if(isset($_GET['sector']))
			$sector = $_GET['sector'];
		elseif(isset($_POST['sector']))
			$sector = $_POST['sector'];
		elseif(isset($_REQUEST['sector']))
			$sector = $_REQUEST['sector'];

		setcookie('sector', $sector);
		return new Sector($sector);
	}

	function get_alliance_members($alliance_id){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM user
			WHERE user_alliance_id = %d',
			$alliance_id
		));
		return $this->get_objects($ret);
	}

	function get_alliance_leaders($alliance_id){
		global $sql;
		$rs = $sql->query(sprintf('
			SELECT * FROM user
			WHERE user_alliance_id = %d',
			$alliance_id
		));
		$ret = array();
		foreach($this->get_objects($rs) as $member){
			if($member->has_permission('ALLIANCE')){
				$ret[] = $member;
			}
		}
		return $ret;
	}

	function render(){
		echo '<a href="send_message.php?raw_to_names=' .
		     $this->name . '&to_step=1">' . htmlentities($this->name) . '</a>';
	}

}

?>
