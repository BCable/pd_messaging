<?
include_once('setup.php');
include_once('classes/user.php');

$user = new User();
$user->login_auth($_REQUEST['auth']);

$history->record();
include_once('header.php');

$expire_in = intval($_REQUEST['expire_in']);

if(
	empty($_REQUEST['send']) ||
	empty($_REQUEST['body']) ||
	empty($_REQUEST['subject']) ||
	$expire_in < 1 || $expire_in > 73
){ ?>
<div class="h2">Send Sector Message (<?=$_REQUEST['sector']?>)</div>
<br />
Note: "Expires" must be an integer between 1 and 72.  This is the length of time in hours.
<br /><br />
<form method="POST">
<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>Subject:</th>
	<td>
		<input type="text" name="subject" value="<?=$_REQUEST['subject']?>" />
	</td>
</tr>
<tr>
	<th>Expires:</th>
	<td>
		<input type="text" name="expire_in"
		 value="<?=$_REQUEST['expire_in']?>" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<textarea name="body"><?=$_REQUEST['body']?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
		<input type="submit" class="normal" name="send" value="Send" />
	</td>
</tr>
</table>
</form>

<? } else {
	include_once('classes/message.php');
	$message = new Message();
	$message->send_sector(
		$user, $_REQUEST['sector'], $expire_in,
		$_REQUEST['subject'], $_REQUEST['body']
	);
	echo 'Message sent successfully!';
}

include_once('footer.php');
?>
