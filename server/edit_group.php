<?
include_once('setup.php');
include_once('classes/group.php');
include_once('classes/user.php');

$user = new User();
$user->login_auth($_REQUEST['auth']);

// retrieve info
if(!empty($_REQUEST['id'])){
	$group = new Group();
	$group->get_by_id($_REQUEST['id']);
	$data = $group->get_array();
	$data['group_users'] =
		str_replace('|', "\n", substr($data['group_users'], 1, -1));
	$_REQUEST = array_merge($data, $_REQUEST);
	$has_id = true;
}

// new record
if(!empty($_REQUEST['save'])){
	// get data from user
	$data = $_REQUEST;
	$data_arr = split("[\r\n,]+", trim($data['group_users']));
	$data['group_users'] = '|' . implode('|', $data_arr) . '|';

	// store as SQL data
	$user_array = $user->get_array();
	$data = array_merge($user_array, $data);

	// create the object
	$group = new Group();
	$group->class_store($data);

	// delete existing DB record
	if($_REQUEST['save'] == 'delete'){
		$group->delete($user);
	}

	// store existing saved DB record
	elseif($has_id){
		$group->update($user);
	}

	// create new DB record
	else {
		$group->create();
	}

	// redirect after created
	header('Location: manage_groups.php');
	die(); // just in case... it's happened before, trust me
}

$history->record();
include_once('header.php');

?>
<script language="javascript">
<!--
function do_delete(){
	if(confirm("Are you sure you wish to delete this group?")){
		var del = document.createElement("input");
		del.type = "hidden";
		del.name = "save";
		del.value = "delete";
		document.getElementsByTagName("form")[0].appendChild(del);
		document.getElementsByTagName("form")[0].submit();
	}
}
//-->
</script>
<div class="h2">Edit Group</div>
<br />
Note: Put each username on a new line and/or separate them by commas, not spaces.
<br /><br />
<form method="POST">
<? if($has_id){ ?>
<input type="hidden" name="id" value="<?=$_REQUEST['id']?>" />
<? } ?>
<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>Name:</th>
	<td>
		<input type="text" name="group_name" value="<?=$data['group_name']?>" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<textarea name="group_users"><?=$data['group_users']?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
<?		if($has_id){ ?>
		<input type="button" class="normal" value="Delete"
		 onclick="do_delete();" />
<?		} ?>
		<input type="submit" class="normal" name="save" value="Save" />
	</td>
</tr>
</table>
</form>
<? include_once('footer.php'); ?>
