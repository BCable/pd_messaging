<?
include_once('setup.php');
include_once('classes/message.php');
include_once('classes/user.php');

$user = new User();
$user->login_auth($_REQUEST['auth']);

// retrieve info
if(!empty($_REQUEST['id'])){
	$message = new Message();
	$message->get_by_id($_REQUEST['id']);
	$data = $message->get_array();
	$_REQUEST = array_merge($data, $_REQUEST);
	$has_id = true;
}

// update record
if(
	!empty($_REQUEST['save']) &&
	!empty($_REQUEST['msg_subject']) &&
	!empty($_REQUEST['msg_body'])
){
	// get data from user
	$data = $_REQUEST;

	// create the object
	$msg = new Message();
	$msg->class_store($data);

	// delete existing DB record
	if($_REQUEST['save'] == 'delete'){
		$msg->delete($user);
		header('Location: menu.php');
	}

	// store existing saved DB record
	else {
		$msg->update($user);
		header("Location: get_message.php?id={$msg->id}");
	}

	// redirect after created
	die(); // just in case... it's happened before, trust me
}

$history->record();
include_once('header.php');

?>
<script language="javascript">
<!--
function do_delete(){
	if(confirm("Are you sure you wish to delete this message?")){
		var del = document.createElement("input");
		del.type = "hidden";
		del.name = "save";
		del.value = "delete";
		document.getElementsByTagName("form")[0].appendChild(del);
		document.getElementsByTagName("form")[0].submit();
	}
}
//-->
</script>
<div class="h2">Edit Message</div>
<br />
<form method="POST">
<input type="hidden" name="id" value="<?=$_REQUEST['id']?>" />
<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>From:</th>
	<td><?=$message->user->name?></td>
</tr>
<tr>
	<th>Subject:</th>
	<td>
		<input type="text" name="msg_subject"
		 value="<?=$data['msg_subject']?>" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<textarea name="msg_body"><?=$data['msg_body']?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
		<input type="button" class="normal" value="Delete"
		 onclick="do_delete();" />
		<input type="submit" class="normal" name="save" value="Save" />
	</td>
</tr>
</table>
</form>
<? include_once('footer.php'); ?>
