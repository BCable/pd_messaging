<?
session_start();
include_once('classes/history.php');
$history = new History();
$history->clear();
unset($_SESSION['incognito']);
unset($_SESSION['admin_username']);
header('Location: menu.php');
?>
