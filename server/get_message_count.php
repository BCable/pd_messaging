<?
include_once('setup.php');
include_once('classes/user.php');
include_once('classes/message.php');

// out of date? then force upgrade
if($_REQUEST['v'] != Config::VERSION){
	$login_auth = false;
	$display = 'UPGRADE';
	$url = Config::SCRIPT_URL;

// otherwise we are fine
} else {
	$user = new User();
	$login_auth = $user->login_auth($_GET['auth'], true);
	if($login_auth) $sector = $user->register_sector();
}

// set up default JS elements ?>
var piam_span = document.getElementById("piam_span");
var piam_a = document.getElementById("piam_a");
<?

// get count of messages not seen yet
if($login_auth){
	$msg = new Message();
	$glob_msg_count = $msg->get_global_cnt_new($user);
	$secure_msg_count = $msg->get_secure_cnt_new($user);
	$sector_msg_count = $msg->get_sector_cnt_new($user, $sector);
	$region_msg_count = $msg->get_region_cnt_new($user, $sector);

	// sum them up
	$tot_count =
		$glob_msg_count + $secure_msg_count +
		$sector_msg_count + $region_msg_count;

	// no messages
	if($tot_count == 0){
		$display = '0';

	// some messages
	} else {
		$display = '';
		if($glob_msg_count > 0){
			$display .=
				'&nbsp;|&nbsp;<span style="color: ' .
					Config::COLOR_GLOBAL .
				"\">{$glob_msg_count}</span>";
		}
		if($sector_msg_count > 0){
			$display .=
				'&nbsp;|&nbsp;<span style="color: ' .
					Config::COLOR_SECTOR .
				"\">{$sector_msg_count}</span>";
		}
		if($region_msg_count > 0){
			$display .=
				'&nbsp;|&nbsp;<span style="color: ' .
					Config::COLOR_REGION .
				"\">{$region_msg_count}</span>";
		}
		if($secure_msg_count > 0){
			$display .=
				'&nbsp;|&nbsp;<span style="color: ' .
					Config::COLOR_SECURE .
				"\">{$secure_msg_count}</span>";
		}

		$display = substr($display, 13);
	}

} elseif(empty($display)) {
	$display = 'ERROR';
}

// send to client
if($tot_count > 0 || $display === 'ERROR' || $display === 'UPGRADE'){ ?>
piam_span.style.fontWeight = "bold";
<? }

// errors show up in red
if($display === 'ERROR' || $display === 'UPGRADE'){ ?>
piam_span.style.color = "#FF0000";
<? }

// if there are no new messages, display in the slightly lighter color
elseif($tot_count == 0) { ?>
piam_span.style.color = "<?=Config::COLOR_SECURE?>";
<? }

// otherwise, we want white
else { ?>
piam_span.style.color = "#FFFFFF";
<? }

// if a URL is set from above, redirect to it
if(isset($url)){ ?>
piam_a.href = "<?=addslashes($url)?>";
<? }

// do display ?>
piam_span.innerHTML = "=&nbsp;<?=addslashes($display)?>&nbsp;=";
<?

// deprecated... but still required for backwards compatibility (this is now in
// the pd_interalliance.data.js file) ?>
var piam_gmc = document.createElement("a");
piam_gmc.id = "piam_gmc";
document.body.appendChild(piam_gmc);
