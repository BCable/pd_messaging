<?
include_once('setup.php');
include_once('classes/message.php');
include_once('classes/paging.php');
include_once('classes/user.php');

$history->record();
$user = new User();
$user->login_auth($_REQUEST['auth']);
$sector = $user->register_sector();

include_once('header.php');

$message = new Message();
$message->get_by_id($_REQUEST['id']); ?>

<div class="h2"><?=$message->get_printable_type()?> Message</div>
<br />

<?
$message->render($user, true);

include('footer.php'); ?>
