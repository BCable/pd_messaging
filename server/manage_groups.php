<?
include_once('setup.php');
include_once('classes/group.php');
include_once('classes/user.php');

$history->record();
$user = new User();
$user->login_auth($_REQUEST['auth']);

include_once('header.php');

$group = new Group();
$groups = $group->get_groups($user);
?>

<div class="fleft">
	<table cellpadding="0" cellspacing="0" class="menu">
	<tr class="title">
		<th>Groups</th>
	</tr>
<?	foreach($groups as $grp){ ?>
	<tr class="middle">
		<td>
			<a href="edit_group.php?id=<?=$grp->id?>">
				<?=$grp->name?>
			</a>
		</td>
	</tr>
<?	} ?>
	<tr class="middle bottom">
		<td>
			<a href="edit_group.php">
				Add&nbsp;New&nbsp;Group
			</a>
		</td>
	</tr>
	</table>
</div>

<? include_once('footer.php'); ?>
