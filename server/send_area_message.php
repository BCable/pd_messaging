<?
include_once('setup.php');
include_once('classes/region.php');
include_once('classes/user.php');

$user = new User();
$user->login_auth($_REQUEST['auth']);
$user->require_perm_array_or(array('SECTOR','REGION'), array(false, false));

$history->record();
include_once('header.php');

if(
	empty($_REQUEST['send']) ||
	empty($_REQUEST['area']) ||
	empty($_REQUEST['body']) ||
	empty($_REQUEST['subject'])
){

	$region = new Region();
	$regions = $region->get_regions_by_permissions($user);

	// calculate expire date
	$expire_in = intval($_REQUEST['expire_in']);
?>
<div class="h2">Send Area Message</div>
<br />
Note: "Expires" is the length of time in hours.  Leave "Expires" blank for them to stay without expiring.
<br /><br />
<form method="POST">
<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>Area:</th>
	<td>
		<select name="area">
<?		foreach($regions as $reg){
			var_dump($reg);
			if(!$reg->no_region){ ?>
			<option value="<?=$reg->id?>"
<?				if($_REQUEST['area']==$reg->id){ ?>
					selected="selected"
<?				} ?>
				><?=$reg->name?> (region)
			</option>
<?			}
			$sectors = explode('|', substr($reg->sectors, 1, -1));
			foreach($sectors as $sector){ ?>
				<option value="<?=$sector?>"
<?					if($_REQUEST['area']==$sector){ ?>
					selected="selected"
<?					} ?>
					><?=$reg->name?> - <?=$sector?> (sector)
				</option>
<?			} ?>
<?		} ?>
		</select>
	</td>
</tr>
<tr>
	<th>Subject:</th>
	<td>
		<input type="text" name="subject" value="<?=$_REQUEST['subject']?>" />
	</td>
</tr>
<tr>
	<th>Expires:</th>
	<td>
		<input type="text" name="expire_in"
		 value="<?=$_REQUEST['expire_in']?>" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<textarea name="body"><?=$_REQUEST['body']?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
		<input type="submit" class="normal" name="send" value="Send" />
	</td>
</tr>
</table>
</form>

<? } else {
	include_once('classes/message.php');
	$message = new Message();

	$numeric_area = preg_replace('/[^0-9]/', '', $_REQUEST['area']);;
	// regional message
	if($numeric_area == $_REQUEST['area']){
		$success = $message->send_region(
			$user, $_REQUEST['area'], $expire_in,
			$_REQUEST['subject'], $_REQUEST['body']
		);
	}

	// sector message
	else {
		$success = $message->send_sector(
			$user, $_REQUEST['area'], $expire_in,
			$_REQUEST['subject'], $_REQUEST['body']
		);
	}

	if($success)
		echo 'Message sent successfully!';
	else
		echo 'Message failed to send.';
}

include_once('footer.php');
?>
