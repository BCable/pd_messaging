<?
include_once('setup.php');
include_once('classes/message.php');
include_once('classes/region.php');
include_once('classes/user.php');

$history->record();
$user = new User();
$user->login_auth($_REQUEST['auth']);
$sector = $user->register_sector();

include_once('header.php');

$msg = new Message();

?>

<div class="fleft">

	<table cellspacing="0" cellpadding="0" class="minimenu">
	<tr class="title">
		<th>
			Incoming
		</th>
	</tr>
	<tr class="middle">
		<td>
			<a style="color: <?=Config::COLOR_GLOBAL?>"
			   href="get_messages.php?type=global">
				<div class="left">Global&nbsp;Messages:</div>
				<div class="right">
				<?
					echo $msg->get_global_cnt_new($user) . '/' .
					     $msg->get_global_cnt();
				?>
				</div>
			</a>
		</td>
	</tr>
	<tr class="middle">
		<td>
			<a style="color: <?=Config::COLOR_SECTOR?>"
			   href="get_messages.php?type=sector">
				<div class="left">Sector&nbsp;Messages:</div>
				<div class="right">
				<?
					echo $msg->get_sector_cnt_new($user,$sector) . '/' .
					     $msg->get_sector_cnt($sector);
				?>
				</div>
			</a>
		</td>
	</tr>
	<tr class="middle">
		<td>
			<a style="color: <?=Config::COLOR_REGION?>"
			   href="get_messages.php?type=region">
				<div class="left">Regional&nbsp;Messages:</div>
				<div class="right">
				<?
					echo $msg->get_region_cnt_new($user,$sector) . '/' .
					     $msg->get_region_cnt($sector);
				?>
				</div>
			</a>
		</td>
	</tr>
	<tr class="middle bottom">
		<td>
			<a style="color: <?=Config::COLOR_SECURE?>"
			   href="get_messages.php?type=secure">
				<div class="left">Secure&nbsp;Messages:</div>
				<div class="right">
				<?
					echo $msg->get_secure_cnt_new($user) . '/' .
					     $msg->get_secure_cnt($user);
				?>
				</div>
			</a>
		</td>
	</tr>
	</table>

	<div class="clrb"><br /></div>

	<table cellspacing="0" cellpadding="0" class="dispmenu">
	<tr class="title">
		<th>
			Information
		</th>
	</tr>
	<tr class="middle">
		<td>
			<div class="left">Detected&nbsp;Sector:</div>
			<div class="right"><?=$sector->name?></div>
		</td>
	</tr>
	<tr class="middle bottom">
		<td>
			<div class="left">Detected&nbsp;Regions:</div>
			<div class="right" style="text-align: right">
			<?
				$region = new Region();
				$regs = $region->get_regions($sector);
				$i = 0;
				foreach($regs as $reg){
					if($i++ != 0){
						echo '<br />';
					}
					echo $reg->name;
				}
				if($i == 0){
					echo '(none)';
				}
			?>
			</div>
		</td>
	</tr>
	</table>

	<div class="clrb"><br /></div>

	<table cellspacing="0" cellpadding="0" class="menu">
	<tr class="title">
		<th>
			Management
		</th>
	</tr>
	<tr class="middle<?=(!$user->is_admin() && !$_SESSION['incognito']?' bottom':null)?>">
		<td>
			<a href="manage_groups.php">
				Manage&nbsp;Your&nbsp;Message&nbsp;Groups
			</a>
		</td>
	</tr>
<?	if($user->is_admin()){ ?>
	<tr class="middle bottom">
		<td>
			<a href="admin.php">
				Administration&nbsp;(ADMIN)
			</a>
		</td>
	</tr>
<?	} ?>
<?	if($_SESSION['incognito']){ ?>
	<tr class="middle bottom">
		<td>
			<a href="admin_logout.php">
				Logout (INCOGNITO)
			</a>
		</td>
	</tr>
<?	} ?>
	</table>

</div>

<div class="fright">

	<table cellspacing="0" cellpadding="0" class="menu fright">
	<tr class="title">
	<th>
			Outgoing
		</th>
	</tr>
	<tr class="middle">
		<td>
			<a href="send_message.php">
				Send&nbsp;Message
			</a>
		</td>
	</tr>
	<tr class="middle<?
		if(
			!$user->has_permission('SECTOR') &&
			!$user->has_permission('REGION') &&
			!$user->has_permission('GLOBAL')
		) echo ' bottom';
	?>">
		<td>
			<a href="send_sector_message.php">
				Send&nbsp;Message&nbsp;to&nbsp;Current&nbsp;Sector
			</a>
		</td>
	</tr>
<?	if($user->has_permission('SECTOR') || $user->has_permission('REGION')){ ?>
	<tr class="middle<?=(!$user->has_permission('GLOBAL')?' bottom':null)?>">
		<td>
			<a href="send_area_message.php">
				Send&nbsp;Message&nbsp;to&nbsp;Area&nbsp;(AREA)
			</a>
		</td>
	</tr>
<?	}
	if($user->has_permission('GLOBAL')){ ?>
	<tr class="middle bottom">
		<td>
			<a href="send_global_message.php">
				Send&nbsp;Global&nbsp;Message&nbsp;(GLOBAL)
			</a>
		</td>
	</tr>
<?	} ?>
	</table>

	<div class="clrb"><br /></div>

	<table cellspacing="0" cellpadding="0" class="menu">
	<tr class="title">
		<th>
			In-Game&nbsp;Links
		</th>
	</tr>
	<tr class="middle">
		<td>
			<a href="http://orion.pardus.at/sendmsg.php?to=undefined&subj=undefined">
				Send&nbsp;In-Game&nbsp;Private&nbsp;Message
			</a>
		</td>
	</tr>
	<tr class="middle">
		<td>
			<a href="http://orion.pardus.at/messages_alliance.php#sendbox">
				Send&nbsp;In-Game&nbsp;Alliance&nbsp;Message
			</a>
		</td>
	</tr>
	<tr class="middle">
		<td>
			<a href="http://orion.pardus.at/messages_private.php">
				Private&nbsp;Messages
			</a>
		</td>
	</tr>
	<tr class="middle bottom">
		<td>
			<a href="http://orion.pardus.at/messages_alliance.php">
				Alliance&nbsp;Messages
			</a>
		</td>
	</tr>
	</table>

	<div class="clrb"><br /></div>

<?	if($user->has_permission('SECTOR') || $user->has_permission('REGION')){ ?>
	<table cellspacing="0" cellpadding="0" class="menu">
	<tr class="title">
		<th>
			All&nbsp;Seeing&nbsp;Eye
		</th>
	</tr>
	<tr class="middle bottom">
		<td>
			<a href="all_area.php">
				All&nbsp;Area&nbsp;Messages&nbsp;(AREA)
			</a>
		</td>
	</tr>
	</table>
<? } ?>

</div>

<? include_once('footer.php'); ?>
