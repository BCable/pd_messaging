<?
include_once('setup.php');
include_once('classes/alliance.php');
include_once('classes/group.php');
include_once('classes/message.php');
include_once('classes/user.php');

$user = new User();
$user->login_auth($_REQUEST['auth']);

$history->record();
include_once('header.php');

// SUPPORT FUNCTIONS {{{

function has_receivers(){
	global $_REQUEST;
	// manual entry
	if(!empty($_REQUEST['raw_to_names']))
		return true;
	// group selection
	if(count($_REQUEST['to_groups'])>0)
		return true;
	// alliance leader selection
	if(count($_REQUEST['to_alliance_ldr'])>0)
		return true;
	// alliance members selection
	if(count($_REQUEST['to_alliance'])>0)
		return true;
	// step 2
	if(!empty($_REQUEST['to']))
		return true;

	return false;
}

function get_receivers(){
	global $_REQUEST;
	$receivers = array();
	// manual entry
	if(!empty($_REQUEST['raw_to_names'])){
		$names = explode(',', $_REQUEST['raw_to_names']);
		foreach($names as $name){
			$receivers[] = trim($name);
		}
	}

	// group selection
	if(count($_REQUEST['to_groups'])>0){
		foreach($_REQUEST['to_groups'] as $group_id){
			$group = new Group();
			$data = $group->get_by_id($group_id);
			$users = explode('|', substr($data->users, 1, -1));
			foreach($users as $user){
				$receivers[] = trim($user);
			}
		}
	}

	// alliance leader selection
	if(count($_REQUEST['to_alliance_ldr'])>0){
		foreach($_REQUEST['to_alliance_ldr'] as $alliance_id){
			$tmpuser = new User();
			$members = $tmpuser->get_alliance_leaders($alliance_id);
			foreach($members as $user){
				$receivers[] = $user->name;
			}
		}
	}

	// alliance member selection
	if(count($_REQUEST['to_alliance'])>0){
		foreach($_REQUEST['to_alliance'] as $alliance_id){
			$tmpuser = new User();
			$members = $tmpuser->get_alliance_members($alliance_id);
			foreach($members as $user){
				$receivers[] = $user->name;
			}
		}
	}

	sort($receivers);
	$receivers = array_unique($receivers);

	return $receivers;
}

function get_methods(){
	global $_REQUEST;
	$methods = array();
	// alliance leader selection
	if(count($_REQUEST['to_alliance_ldr'])>0){
		$methods[] = 'Alliance Leaders';
	}

	// alliance member selection
	if(count($_REQUEST['to_alliance'])>0){
		$methods[] = 'Alliance Members';
	}

	// group selection
	if(count($_REQUEST['to_groups'])>0){
		$methods[] = 'Group Selection';
	}

	// manual entry
	if(!empty($_REQUEST['raw_to_names'])){
		$methods[] = 'Manual Entry';
	}

	return $methods;
}

// }}}

// STEP 1 {{{

if(empty($_REQUEST['to_step']) && !has_receivers()){

	$group = new Group();
	$groups = $group->get_groups($user);

	$alliance = new Alliance();
	$alliances = $alliance->get_alliances();

?>
<div class="h2">Step 1: Select Destination</div>
<br />
All messages are delivered internally in the system first.  The people without accounts in the system are sent a PM in game.  If you wish ALL messages to be sent as a PM within the system, then select the "Force PM" option.
<br /><br />
<form method="POST">
<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>People:</th>
	<td><input type="text" name="raw_to_names" class="send_msg m0p0" />
</tr>
<tr>
	<th>Groups:</th>
	<td>
		<select name="to_groups[]" multiple="multiple" size="5"
		 class="send_msg">
<?			foreach($groups as $grp){ ?>
			<option value="<?=$grp->id?>"><?=$grp->name?></option>
<?			} ?>
		</select>
	</td>
</tr>
<? if($user->has_permission("ALLIANCE")){ ?>
<tr>
	<th>Alliance Leaders:</th>
	<td>
		<select name="to_alliance_ldr[]" multiple="multiple" size="5"
		 class="send_msg">
<?			foreach($alliances as $ally){ ?>
				<option value="<?=$ally->id?>"><?=$ally->name?></option>
<?			} ?>
		</select>
	</td>
</tr>
<tr>
	<th>All Alliance Members:</th>
	<td>
		<select name="to_alliance[]" multiple="multiple" size="5"
		 class="send_msg">
<?			foreach($alliances as $ally){ ?>
				<option value="<?=$ally->id?>"><?=$ally->name?></option>
<?			} ?>
		</select>
	</td>
</tr>
<? } ?>
<tr>
	<th>Force&nbsp;PM:</th>
	<td>
		<input type="checkbox" name="force_pm" value="1" />
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
		<input type="submit" class="normal" name="to_step" value="Next" />
	</td>
</tr>
</table>
</form>
<?

}

// }}}

// STEP 2 {{{

elseif(
	empty($_REQUEST['send']) ||
	empty($_REQUEST['subject']) ||
	empty($_REQUEST['body'])
){
	$receivers = get_receivers();
	$methods = get_methods();
	$methods = implode(', ', $methods);
	$text_receivers = implode(', ', $receivers);
	?>

<form method="POST">

<? if(!empty($_REQUEST['force_pm'])){ ?>
<input type="hidden" name="force_pm" value="1" />
<? } ?>
<input type="hidden" name="methods" value="<?=$methods?>" />

<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>To:</th>
	<td>
		<input type="text" name="to" value="<?=$text_receivers?>" />
	</td>
</tr>
<tr>
	<th>Subject:</th>
	<td>
		<input type="text" name="subject" value="<?=$_REQUEST['subject']?>" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<textarea name="body"><?=$_REQUEST['body']?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
		<input type="submit" class="normal" name="send" value="Send" />
	</td>
</tr>
</table>

</form>

<? } else {

// }}}

// STEP 3 {{{

$to_arr = split(',', $_REQUEST['to']);
$message = new Message();
$failures = array(); // these will be sent client side through the in-game
                     // messaging system, if requested by the user

foreach($to_arr as $to){
	$to = trim($to);

	if($_REQUEST['force_pm']){
		$ret = false;
	} else {
		$ret = $message->send_regular(
			$user, $to, $_REQUEST['to'], $_REQUEST['methods'],
			$_REQUEST['subject'], $_REQUEST['body']
		);
	}

	if(!$ret){
		$failures[] = $to;
	}
}

if(count($failures)==0){
	echo 'Message was sent successfully!';
} else {

	function js_sanitize($str){
		$str = addslashes($str);
		$str = str_replace("\n", '\n', $str);
		$str = str_replace("\r", '', $str);
		return $str;
	}
	?>

<script language="javascript">
<!--
var subject = "<?=js_sanitize($_REQUEST['subject'])?>";
var body = "<?=js_sanitize($_REQUEST['body'])?>";
var users = new Array(
<?
$first = true;
foreach($failures as $failure){
	if($first) $first = false;
	else echo ',';
	echo '"'.js_sanitize($failure).'"';
} ?>
);

body += "\n";
body += "\n------------------------------------------------------------------";
body += "\n This message was sent via the Interalliance Communication System";

function send_messages(){
	var send = document.getElementById('js_send');
	send.innerHTML = "Sending extra messages...";

	for(var i in users){
		// iframe
		var iframe = document.createElement('iframe');
		iframe.style.display = "none";
		document.body.appendChild(iframe);

		// form
		var form = document.createElement("form");
		form.method = "POST";
		form.action = "http://<?=$user->universe?>.pardus.at/sendmsg.php";

		// recipient
		var in1 = document.createElement("input");
		in1.type = "text";
		in1.name = "recipient";
		in1.value = users[i];
		form.appendChild(in1);

		// subject
		var in2 = document.createElement("input");
		in2.type = "text";
		in2.name = "textfield";
		in2.value = subject;
		form.appendChild(in2);

		// body
		var in3 = document.createElement("textarea");
		in3.name = "textarea";
		in3.value = body;
		form.appendChild(in3);

		// attach signature field
		var in4 = document.createElement("input");
		in4.type = "text";
		in4.name = "attach_signature";
		in4.value = "attach_signature";
		form.appendChild(in4);

		// submit button
		var in5 = document.createElement("input");
		in5.type = "hidden";
		in5.name = "Send";
		in5.value = "Send";
		form.appendChild(in5);

		// append and execute
		iframe.contentDocument.body.appendChild(form);
		form.submit();
	}
	send.innerHTML = "Sending extra messages... Sent!";
}

window.addEventListener('load', send_messages, false);

//-->
</script>

<div id="js_send" style="font-weight: bold">&nbsp;</div>
<br />
Message was sent successfully.
<br /><br />
The following users are now being messaged in game because they were not found in the system or it was requested.
<br />
<? foreach($failures as $fail){
	echo "<br />{$fail}";
} ?>

<?
}

// }}}

}

include_once('footer.php'); ?>
