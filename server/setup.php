<?
session_start();

include_once('magic_quotes.php');
include_once('classes/history.php');
include_once('classes/config.php');
include_once('classes/quicksql.php');

$sql = new QuickSQL();
$sql->connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASS);
$sql->select_db(Config::DB_NAME);

$history = new History();
if($_REQUEST['fresh'] == 1)
	$history->clear();

?>
