<?
include_once('setup.php');
include_once('classes/user.php');

$user = new User();
$user->login_auth($_REQUEST['auth']);
$user->require_perm('ADMIN');

$history->record();
include_once('header.php');

if(
	empty($_REQUEST['send']) ||
	empty($_REQUEST['body']) ||
	empty($_REQUEST['subject'])
){
?>

<form method="POST">
<table cellpadding="0" cellspacing="0" class="input_form">
<tr>
	<th>Subject:</th>
	<td>
		<input type="text" name="subject" value="<?=$_REQUEST['subject']?>" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<textarea name="body"><?=$_REQUEST['body']?></textarea>
	</td>
</tr>
<tr>
	<td colspan="2" style="text-align: center">
		<input type="submit" class="normal" name="send" value="Send" />
	</td>
</tr>
</table>
</form>

<? } else {
	include_once('classes/message.php');
	$message = new Message();
	$ret = $message->send_global(
		$user, $_REQUEST['subject'], $_REQUEST['body']
	);
	if($ret) echo 'Message sent successfully!';
	else echo 'Message failed to send.';
}

include_once('footer.php');
?>
