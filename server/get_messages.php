<?
include_once('setup.php');
include_once('classes/message.php');
include_once('classes/paging.php');
include_once('classes/user.php');

$history->record();
$user = new User();
$user->login_auth($_REQUEST['auth']);
$sector = $user->register_sector();

include_once('header.php');

$paging = new Paging($_GET['p'], $_GET['limit']);
$msg = new Message();
$msg->set_paging($paging);

// set total
switch($_GET['type']){
	// global messages
	case 'global':
		$type_name = 'Global';
		$paging->set_total($msg->get_global_cnt());
		$messages = $msg->get_global();

		// update user's date
		$user->update_global();
		break;

	// sector messages
	case 'sector':
		$type_name = 'Sector';
		$paging->set_total($msg->get_sector_cnt($sector));
		$messages = $msg->get_sector($sector);

		// mark messages as read
		$msg->mark_all_read($user, $messages);
		break;

	// regional messages
	case 'region':
		$type_name = 'Regional';
		$paging->set_total($msg->get_region_cnt($sector));
		$messages = $msg->get_region($sector);

		// mark messages as read
		$msg->mark_all_read($user, $messages);
		break;

	// secure messages
	case 'secure':
		$type_name = 'Secure';
		$paging->set_total($msg->get_secure_cnt($user));
		$messages = $msg->get_secure($user);

		// mark messages as read
		$msg->mark_all_read($user, $messages);
		break;

	// fail
	default:
		die('ERROR');
		break;
}

?>

<div class="h2"><?=$type_name?>&nbsp;Messages</div>
<br />

<div id="wrapper">

<?
foreach($messages as $msg){
	$msg->render($user);
}

$paging->print_page_trail('',
	"type={$_GET['type']}" . (
		isset($_REQUEST['limit'])?"&limit={$_REQUEST['limit']}": null
	)
);

// unrelated: display a link to expand pages
if(!isset($_GET['limit'])){
	echo '<br /><br />';
	echo "
	<a href=\"?p={$paging->page}&limit=10&type={$_GET['type']}\">
		10 items per page
	</a>";
} ?>

</div>

<br /><br />

<? include_once('footer.php'); ?>
