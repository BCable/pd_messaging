<?
include_once('setup.php');
include_once('classes/message.php');
include_once('classes/region.php');
include_once('classes/user.php');

$history->record();
$user = new User();
$user->login_auth($_REQUEST['auth']);
$user->require_perm_array_or(array('SECTOR','REGION'), array(false, false));

include_once('header.php');

$region = new Region();
$regions = $region->get_regions_by_permissions($user);

$message = new Message();

?>

<script language="javascript">
<!--

function toggle_region(region){
	var span = document.getElementById(region);
	var disp;
	if(span.innerHTML == "-"){
		span.innerHTML = "+";
		disp = "none";
	} else {
		span.innerHTML = "-";
		disp = "table-row";
	}

	var sectors = document.getElementsByName(region);
	for(var i in sectors){
		sectors[i].style.display = disp;
	}
}

//-->
</script>

<table cellpadding="0" cellspacing="0" class="message_list">

<tr>
	<td></td>
	<td style="color: <?=Config::COLOR_REGION?>">Region</td>
	<td>|</td>
	<td class="right" style="color: <?=Config::COLOR_SECTOR?>">Sector</td>
</tr>

<?

function fix($str){
	$str = str_replace(' ', '&nbsp;', $str);
	return $str;
}

foreach($regions as $reg){
	$sectors = explode('|', substr($reg->sectors, 1, -1));
	$sector = new Sector($sectors[0]);
	$message = new Message();
	$region_messages = $message->get_region($sector);
	$region_message_count = count($region_messages);

	// loop sector messages to get total, and later for display
	$sector_total = 0;
	$sector_msgs = array();
	foreach($sectors as $sector_name){
		$sector = new Sector($sector_name);
		$sector_msgs[$sector->name] = $message->get_sector($sector);
		$sector_total += count($sector_msgs[$sector->name]);
	}

	// get region name for tree view javascript
	$js_name = preg_replace('/[^a-z0-9]/', '', strtolower($reg->name));
?>

<tr class="region">
	<th style="color: <?=Config::COLOR_REGION?>; cursor: pointer"
	 onclick="toggle_region('<?=$js_name?>');"
	 ><?=fix($reg->name)?>&nbsp;[<span id="<?=$js_name?>">+</span>]</th>
	<td style="color: <?=Config::COLOR_REGION?>"><?=$region_message_count?></td>
	<td>|</td>
	<td class="right"
	 style="color: <?=Config::COLOR_SECTOR?>"><?=$sector_total?></td>
</tr>

<?	foreach($region_messages as $msg){
	?>
<tr class="rmessage nope" name="<?=$js_name?>">
	<th><?=htmlentities($msg->subject)?>&nbsp;(region)</th>
	<td><a href="get_message.php?id=<?=$msg->id?>">View</a></td>
	<td>|</td>
	<td class="right"><a href="edit_message.php?id=<?=$msg->id?>">Edit</a></td>
</tr>
<?	}

	foreach($sectors as $sector_name){
		$sector = new Sector($sector_name);
		$messages = $sector_msgs[$sector->name];
		$message_count = count($messages);
?>
<tr class="sector nope" name="<?=$js_name?>">
	<th style="color: <?=Config::COLOR_SECTOR?>"><?=$sector->name?></th>
	<td style="color: <?=Config::COLOR_REGION?>">-</td><td>|</td>
	<td class="right" style="color: <?=Config::COLOR_SECTOR?>"><?=$message_count?></td>
</tr>
<?		foreach($messages as $msg){ ?>
<tr class="message nope" name="<?=$js_name?>">
	<th><?=htmlentities($msg->subject)?>&nbsp;(sector)</th>
	<td><a href="get_message.php?id=<?=$msg->id?>">View</a></td>
	<td>|</td>
	<td class="right"><a href="edit_message.php?id=<?=$msg->id?>">Edit</a></td>
</tr>
<?		}
	}
} ?>

</table>
