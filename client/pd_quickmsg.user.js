<? header("Content-Type: text/javascript"); ?>
// ==UserScript==
// @name        Pardus Quick Messaging
// @namespace   http://bcable.net/
// @version     2.0.0
// @description Loads PMs, AMs, and potentially interalliance messages in an
//              iframe for quick access
// @include     http://*.pardus.at/msgframe.php
// @copyright   2010, Brad Cable
// @license     Modified BSD: http://bcable.net/license.php
// ==/UserScript==

// config
var box_width = "650px";
var box_height = "475px";
var enable_hooking = true;

// get universe name and username
var infoImg = document.getElementsByTagName("img")[0];
var universe = infoImg.alt.replace(/^(Artemis|Orion|Pegasus): .*/, "$1");
universe = universe.toLowerCase();
var username = infoImg.alt.replace(/^(Artemis|Orion|Pegasus): /, "");
username = username.replace(" ", "%20");

// get URLs
var msg_url_private = "http://" + universe +
	".pardus.at/messages_private.php?sort=received&amp;page=1";
var msg_url_alliance = "http://" + universe +
	".pardus.at/messages_alliance.php?sort=all&amp;page=1";

// create and display iframe
function draw(url){
	var a = document.createElement("a");
	a.id = "pd_quickmsg";
	a.style.position = "absolute";
	a.style.top = parent.frames[2].scrollY + "px";
	a.style.left = (window.innerWidth - parseInt(box_width) - 20) + "px";
	a.style.width = box_width;
	a.style.height = box_height;
	a.style.zIndex = 99;
	a.style.margin = "0px";
	a.style.padding = "0px";
	a.style.border = "10px solid #FF0000";
	a.style.backgroundColor = "#00001C";
	a.addEventListener("click", kill, false);

	var ifrm = document.createElement("iframe");
	ifrm.src = url;
	ifrm.style.border = "0px";
	ifrm.style.width = box_width;
	ifrm.style.height = box_height;
	ifrm.style.backgroundColor = "#00001C";

	a.appendChild(ifrm);
	parent.frames[2].document.body.appendChild(a);
	location.reload();
}

// destroy all iframes
function kill(){
	var pd_quickmsg = true;
	while(pd_quickmsg){
		pd_quickmsg =
			parent.frames[2].document.getElementById("pd_quickmsg");
		parent.frames[2].document.body.removeChild(pd_quickmsg);
	}
}

// only if enabled
if(enable_hooking){
	// hook alliance messages
	var pm = document.getElementById("new_msg");
	if(pm){
		var pma = pm.parentNode;
		pma.target = "_self";
		pma.href = "#";
		pma.addEventListener("click", function(){
			draw(msg_url_private);
		},false);
	}

	// hook alliance messages
	var ally = document.getElementById("new_amsg");
	if(ally){
		var allya = ally.parentNode;
		allya.target = "_self";
		allya.href = "#";
		allya.addEventListener("click", function(){
			draw(msg_url_alliance);
		},false);
	}
}

// hook PIAM messages (another script of mine)
function hook_piam(){
	var time_spent = 0;
	var interval = setInterval(function(){
		// time out after 10 seconds
		time_spent += 200;
		if(time_spent > 10000){
			clearInterval(interval);
		}

		// hook PIAM
		if(document.getElementById("loaded")){
			var piam_gmc = document.getElementById("piam_gmc");
			if(piam_gmc){
				var piam_a = document.getElementById("piam_a");
				var url = piam_a.href;

				// this line makes double certain that the URL becomes "#",
				// since it checks 200 milliseconds later
				if(url.substr(-1) == "#") clearInterval(interval);

				else {
					piam_a.target = "_self";
					piam_a.href = "#";
					piam_a.addEventListener("click", function(){
						draw(url);
					},false);
				}
			}
		}
	},200);
}
// if the PIAM script is already starting loaded
if(document.getElementById("piam")) hook_piam();
// if the script hasn't, but we still want to check after page load just in case
else window.addEventListener("load", hook_piam, false);
