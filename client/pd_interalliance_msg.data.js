// ==UserScript==
// @name        Pardus Interalliance Message Receiver
// @namespace   http://bcable.net/
// @version     2.1.0
// @description Loads interalliance messages into the message frame bar
// @include     http://*.pardus.at/*.php
// @copyright   2010, Brad Cable
// @license     Modified BSD: http://bcable.net/license.php
// ==/UserScript==

// config
var site_location = {
	"orion": {
		"password": "<PASSWORD>",
		"site": "http://orion.plimini.net/piamv2"
	}
};

// create trim() method
function trim(str){
	return str.replace(/^\s+|\s+$/g, "");
}

// verify the page has loaded; this is a weird hack I know, but it's the only
// thing that actually works consistently
if(location.href.indexOf("msgframe.php") == -1){
	window.addEventListener("load", function(){
		var loaded = document.createElement("a");
		loaded.id = "loaded";
		document.body.appendChild(loaded);
	},false);

// for msgframe.php (note: this does NOT store a "loaded" tag immediately)
} else {
	// announce presence for quickmsg script
	var piam = document.createElement("a");
	piam.id = "piam";
	document.body.appendChild(piam);

	// get universe name and username
	var infoImg = document.getElementsByTagName("img")[0];
	var universe = infoImg.alt.replace(/^(Artemis|Orion|Pegasus): .*/, "$1");
	universe = universe.toLowerCase();
	var username = infoImg.alt.replace(/^(Artemis|Orion|Pegasus): /, "");
	username = username.replace(" ", "%20");

	// this is in a function because sometimes it needs to be called on frame
	// load, sometimes it needs to be called after... depends on the situation
	function do_everything_important(){
		function cookie_yank(cookname){
			var cookreg = new RegExp(".*" + cookname + "=([^;]*);.*");
			if(!document.cookie.match(cookreg)){
				// last cookie set?
				var cookreg = new RegExp(".*" + cookname + "=([^;]*)$");
				if(document.cookie.match(cookreg)){
					var cookie_val = document.cookie.replace(cookreg, "$1");
					return trim(cookie_val);
				}
				else return false;
			}
			var cookie_val = document.cookie.replace(cookreg, "$1");
			return trim(cookie_val);
		}
		function cookie_push(cookname, cookval, time){
			var multiplier = 1;
			var expires = new Date();
			// it seems that at least some implementations use microseconds, not
			// seconds...
			if(expires.getTime().toString().length == 13)
				multiplier = 1000;

			expires.setTime(expires.getTime()+1000*time);
			document.cookie =
				trim(cookname) + "=" + trim(cookval) +
				"; expires=" + expires.toGMTString() + ";";
		}

		var sector = parent.frames[2].document.getElementById("sector");
		// nav screen
		if(sector){
			sector = sector.textContent;
		// docked ship screen
		} else {
			var tbl =
				parent.frames[2].document.getElementsByTagName("table");
			if(tbl.length > 3){
				tbl = tbl[3];
				tbl = tbl.getElementsByTagName("td");
				if(tbl.length > 5){
					if(tbl[4].textContent == "Sector:"){
						sector = tbl[5].textContent;
						sector =
							sector.replace("&nbsp;","").replace("&#160;","");
					}
				}
			}
		}
		// retrieve from cookie if value cannot be determined immediately
		if(!sector || sector == "unknown"){
			var cookie_sector = cookie_yank("piam_sector");
			if(
				cookie_sector != null &&
				cookie_sector.match(/^[A-Za-z0-9\- ]*$/)
			)
				sector = cookie_sector;
			else
				sector = "unknown";
		}
		sector = trim(sector);
		document.cookie = "piam_sector=" + sector + ";";

		// draw main link
		if(site_location[universe]["site"]){
			// auth string
			var auth = window.btoa(
				username + ":" + site_location[universe]["password"]
			);

			// create msgframe tag
			var piam_a = document.createElement("a");
			piam_a.id = "piam_a";
			piam_a.target = "_new";
			piam_a.href = site_location[universe]["site"] +
				"/menu.php?auth=" + auth +
				"&uni=" + universe + "&sector=" + sector + "&fresh=1";
			var table = document.getElementsByTagName("table")[0];
			var td = table.childNodes[0].childNodes[0].childNodes[3];
			piam_a.style.marginLeft = "10px";

			// get from cache
			if(cookie_yank("piam_ahref") && cookie_yank("piam_span")){
				piam_a.href = unescape(cookie_yank("piam_ahref"));
				piam_a.innerHTML = unescape(cookie_yank("piam_span"));
				td.appendChild(piam_a);

				// mark as loaded
				var piam_gmc = document.createElement("a");
				piam_gmc.id = "piam_gmc";
				document.body.appendChild(piam_gmc);

			} else {
				// fresh reload
				var piam_span = document.createElement("span");
				piam_span.id = "piam_span";
				piam_span.style.fontSize = "12pt";
				piam_span.innerHTML = "...loading...";
				piam_a.appendChild(piam_span);
				td.appendChild(piam_a);

				// request the server info only if cache is empty
				var script = document.createElement("script");
				script.id = "piam_script";
				script.defer = "defer";
				script.src = site_location[universe]["site"] +
					"/get_message_count.php?auth=" + auth +
					"&uni=" + universe + "&sector=" + sector + "&v=2.1.0";
				script.addEventListener("load", function(){
					// set cache
					cookie_push("piam_span", escape(piam_a.innerHTML), 25);
					cookie_push("piam_ahref", escape(piam_a.href), 25);
					// mark as loaded
					var piam_gmc = document.createElement("a");
					piam_gmc.id = "piam_gmc";
					document.body.appendChild(piam_gmc);
				},false);
				document.getElementsByTagName("head")[0].appendChild(script);
			}
		}

		// create refresh button
		var td = document.getElementsByTagName("td")[0];
		var br = td.getElementsByTagName("br")[0];
		var text = document.createTextNode(" | ");
		var refresh = document.createElement("a");
		refresh.href = "#";
		refresh.innerHTML = "Refresh";
		refresh.addEventListener("click", function(){
			location.reload();
		},false);
		td.insertBefore(text, br);
		td.insertBefore(refresh, br);

		// announce to quickmsg script
		var loaded = document.createElement("a");
		loaded.id = "loaded";
		document.body.appendChild(loaded);
	}

	// do everything important
	if(parent.frames[2].document.getElementById("loaded")){
		do_everything_important();
	} else {
		parent.frames[2].window.addEventListener("load",
			do_everything_important, false
		);
	}
}
