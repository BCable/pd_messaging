<?php

function reject(){
	header('WWW-Authenticate: Basic realm="My Realm"');
	header('HTTP/1.0 401 Unauthorized');
	echo 'Text to send if user hits Cancel button';
	exit;
}

if (!isset($_SERVER['PHP_AUTH_USER'])) {
	reject();

} else {
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Description: File Transfer");
	header("Content-Type: text/javascript");
	$contents = file_get_contents("pd_interalliance_msg.data.js");
	$contents = str_replace("<PASSWORD>", $_SERVER['PHP_AUTH_PW'], $contents);
	echo $contents;
}

?>
