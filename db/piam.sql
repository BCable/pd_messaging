-- phpMyAdmin SQL Dump
-- version 3.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 02, 2011 at 02:14 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6-1+lenny9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pardus_orion_piam_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `alliance`
--

CREATE TABLE IF NOT EXISTS `alliance` (
  `alliance_id` int(4) NOT NULL auto_increment,
  `alliance_name` text,
  PRIMARY KEY  (`alliance_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `group_id` int(8) NOT NULL auto_increment,
  `group_name` text,
  `group_user_id` int(6) default NULL,
  `group_users` text,
  PRIMARY KEY  (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `msg_id` int(8) NOT NULL auto_increment,
  `msg_from_id` int(6) default NULL,
  `msg_to_id` int(6) default NULL,
  `msg_recipients` text,
  `msg_methods` text,
  `msg_sector` text,
  `msg_region_id` int(3) default NULL,
  `msg_date` int(10) default NULL,
  `msg_expire_on` int(10) default NULL,
  `msg_seen` text,
  `msg_subject` text,
  `msg_body` text,
  PRIMARY KEY  (`msg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=204 ;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `perm_id` int(8) NOT NULL auto_increment,
  `perm_user_id` int(8) default NULL,
  `perm_name` text,
  `perm_data` text,
  PRIMARY KEY  (`perm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `region_id` int(3) NOT NULL auto_increment,
  `region_name` text,
  `region_sectors` text,
  PRIMARY KEY  (`region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(6) NOT NULL auto_increment,
  `user_universe` text,
  `user_name` text,
  `user_alliance_id` int(4) default NULL,
  `user_pass` text,
  `user_last_update` int(10) default NULL,
  `user_ips` text,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;
